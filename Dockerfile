FROM rust:1.76.0-bookworm 
WORKDIR /app
COPY . .
RUN cargo build --release
CMD ["sh","-c","./target/release/image-chat-backend ${BACKEND}"]
