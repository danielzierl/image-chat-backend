use crate::structs::enums::ModelBackend;

pub fn check_env(backend: &ModelBackend) {
    match backend {
        ModelBackend::Aws => {
            if std::env::var("AWS_API_KEY").is_err() {
                panic!("AWS_ACCESS_KEY_ID is not set");
            }
        }
        ModelBackend::Anthropic => {
            if std::env::var("ANTHROPIC_API_KEY").is_err() {
                panic!("ANTHROPIC_API_KEY is not set");
            }
        }
    }
}
