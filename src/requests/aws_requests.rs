use serde::{Deserialize, Serialize};

use crate::structs::anthropic_api_content::AwsBackendRequestBody;

#[derive(Deserialize, Serialize)]
struct AwsResponse {
    response_message: String,
}

pub async fn send_aws_request(mut body: AwsBackendRequestBody) -> String {
    let url = "https://vc3lv54ss9.execute-api.us-west-2.amazonaws.com/DEV/process_image";
    let client = reqwest::Client::new();
    let aws_api_key = match std::env::var("AWS_API_KEY") {
        Ok(key) => key,
        Err(err) => panic!("Error loading api key: {}", err),
    };
    body.password = Some(aws_api_key);
    let body_str = serde_json::to_string(&body).unwrap();
    let req_builder = client
        .post(url)
        .header("content-type", "application/json")
        .body(body_str);
    let res_wrp = req_builder.send();
    let res_text = res_wrp.await.unwrap().text().await.unwrap();

    let aws_resp: AwsResponse = serde_json::from_str(&res_text).unwrap();
    aws_resp.response_message
}
