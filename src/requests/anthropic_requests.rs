use crate::structs::anthropic_api_content::AnthropicBackendRequestBody;

pub async fn send_anthropic_request(body: AnthropicBackendRequestBody) -> String {
    let url = "https://api.anthropic.com/v1/messages";
    let client = reqwest::Client::new();
    let anthropic_api_key = match std::env::var("ANTHROPIC_API_KEY") {
        Ok(key) => key,
        Err(err) => panic!("Error loading api key: {}", err),
    };
    let body_str = serde_json::to_string(&body).unwrap();
    let req_builder = client
        .post(url)
        .header("x-api-key", anthropic_api_key)
        .header("content-type", "application/json")
        .header("anthropic-version", "2023-06-01")
        .body(body_str);
    let res_wrp = req_builder.send();
    res_wrp.await.unwrap().text().await.unwrap()
}
