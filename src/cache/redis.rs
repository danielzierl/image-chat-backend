pub fn connect_redis() -> redis::RedisResult<redis::Client> {
    let redis_url = std::env::var("REDIS_URL").expect("REDIS_URL must be set");

    let redis_client = redis::Client::open(redis_url)?;
    Ok(redis_client)
}
