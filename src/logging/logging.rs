use std::{
    fs::File,
    io::{BufWriter, Write},
};

pub const SAVE_FILE_NAME_REQUEST: &str = "./static_data/request.txt";
pub const SAVE_FILE_NAME_RESPONSE: &str = "./static_data/response.txt";

pub fn log_into_file(file_name: &str, logged_str: &str) {
    let mut file = BufWriter::new(
        File::options()
            .create(true)
            .append(true)
            .open(file_name)
            .unwrap(),
    );
    let body_str: String = format!("{:?}\n", logged_str);
    let _ = file.write_all(body_str.as_bytes());
}
pub fn convert_t_into_string<T>(body: &T) -> String
where
    T: std::fmt::Debug,
{
    let body_str: String = format!("{:?}\n", body);
    body_str
}
