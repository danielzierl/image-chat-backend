use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AnthropicApiMessage {
    pub role: String,
    pub content: Vec<Message>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum Message {
    #[serde(rename = "text")]
    Text(TextMessage),
    #[serde(rename = "image")]
    Image(ImageMessage),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TextMessage {
    // #[serde(rename = "type")]
    // pub text_type: String,
    pub text: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ImageMessage {
    // #[serde(rename = "type")]
    // pub image_type: String,
    pub source: Source,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Source {
    #[serde(rename = "type")]
    pub source_type: String,
    pub media_type: String,
    pub data: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AwsBackendRequestBody {
    pub messages: Vec<AnthropicApiMessage>,
    pub language_in: String,
    pub language_out: String,
    pub max_length: i32,
    pub password: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AnthropicBackendRequestBody {
    pub model: String,
    pub max_tokens: i32,
    pub messages: Vec<AnthropicApiMessage>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ThisBackendResponse {
    pub prompt: Option<String>,
    pub content: Vec<TextMessage>,
}
