use std::fmt::Debug;

#[derive(Debug)]
pub enum ModelBackend {
    Aws,
    Anthropic,
}

impl Clone for ModelBackend {
    fn clone(&self) -> Self {
        match self {
            ModelBackend::Aws => ModelBackend::Aws,
            ModelBackend::Anthropic => ModelBackend::Anthropic,
        }
    }
}
