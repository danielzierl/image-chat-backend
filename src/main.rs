mod cache;
mod logging;
mod requests;
mod structs;
mod tools;

use redis::Commands;
use std::sync::Arc;

use actix_web::{http::header, web, App, HttpResponse, HttpServer, Responder};
use dotenv::dotenv;
use structs::{
    anthropic_api_content::{AnthropicBackendRequestBody, AwsBackendRequestBody},
    enums::ModelBackend,
};

use crate::requests::{anthropic_requests, aws_requests};
use crate::structs::anthropic_api_content::{Message, TextMessage, ThisBackendResponse};
use crate::tools::hash::hash_string_sha256;
use crate::{
    logging::logging::{
        convert_t_into_string, log_into_file, SAVE_FILE_NAME_REQUEST, SAVE_FILE_NAME_RESPONSE,
    },
    tools::env::check_env,
};

fn config(cfg: &mut web::ServiceConfig) {
    cfg.route("/api/v1/process_image", web::post().to(post_ai_request));
    cfg.route("/api/v1/ping", web::get().to(ping));
}

struct AppState {
    backend: ModelBackend,
    redis: Option<redis::Client>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // let the user select the backend from argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        println!("Usage: cargo run -- [backend]");
        std::process::exit(1);
    };
    let backend = match args[1].as_str() {
        "aws" => ModelBackend::Aws,
        "anthropic" => ModelBackend::Anthropic,
        _ => ModelBackend::Aws,
    };
    println!("BACKEND: {:?}", backend);

    dotenv().ok();
    check_env(&backend); // this panics if the env variable for the current service is not set

    let mut redis: Option<redis::Client> = None;

    if std::env::var("REDIS_URL").is_ok() {
        println!("Using Redis.");
        println!("REDIS_URL: {}", std::env::var("REDIS_URL").unwrap());
        redis = Some(cache::redis::connect_redis().expect("failed to connect to redis"));
    }

    return HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(AppState {
                backend: backend.clone(),
                redis: redis.clone(),
            }))
            .configure(config)
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await;
}

async fn ping() -> impl Responder {
    HttpResponse::Ok().body("pong")
}

async fn post_ai_request(
    body: web::Json<AwsBackendRequestBody>,
    app_state: web::Data<AppState>,
) -> impl Responder {
    println!("GOT REQUEST");
    let inner_body = body.into_inner();
    let body_str = convert_t_into_string(&inner_body);
    let body_str_hash = hash_string_sha256(&body_str);
    // println!("BODY: {}", body_str_hash);

    // if already in cache, return the value in cache
    //

    if app_state.redis.is_some() {
        let response_cache = app_state
            .redis
            .clone()
            .unwrap()
            .get::<&str, String>(&format!("response_{}", body_str_hash));

        if response_cache.is_ok() {
            println!("ALREADY IN CACHE");
            return HttpResponse::Ok().body(response_cache.unwrap());
        }
    }

    let first_message_prompt: &Message = &inner_body.messages[0].content[1];
    let fist_message_prompt_str: String = match first_message_prompt {
        Message::Text(text) => {
            println!("PROMPT: {}", text.text);
            text.text.clone()
        }
        _ => "".to_string(),
    };
    log_into_file(SAVE_FILE_NAME_REQUEST, &body_str);
    let res: ThisBackendResponse = match app_state.backend {
        ModelBackend::Aws => {
            let new_body: AwsBackendRequestBody = inner_body;
            println!("SENDING REQUEST AWS");
            let aws_resp = aws_requests::send_aws_request(new_body).await;
            ThisBackendResponse {
                prompt: Some(fist_message_prompt_str),
                content: vec![TextMessage { text: aws_resp }],
            }
        }
        ModelBackend::Anthropic => {
            let new_body: AnthropicBackendRequestBody = AnthropicBackendRequestBody {
                model: "claude-3-opus-20240229".to_string(),
                max_tokens: inner_body.max_length,
                messages: inner_body.messages.clone(),
            };
            println!("SENDING REQUEST ANTHROPIC ");
            let anthropic_resp = anthropic_requests::send_anthropic_request(new_body).await;
            let mut parsed = serde_json::from_str::<ThisBackendResponse>(&anthropic_resp).unwrap();
            parsed.prompt = Some(fist_message_prompt_str);
            parsed
        }
    };

    println!("GOT RESPONSE");
    let response_str = convert_t_into_string(&res);
    log_into_file(SAVE_FILE_NAME_RESPONSE, &response_str);

    // save to cache
    if app_state.redis.is_some() {
        let app_state = Arc::clone(&app_state);
        let res = res.clone();
        tokio::spawn(async move {
            app_state
                .redis
                .clone()
                .unwrap()
                .set_ex::<&str, String, ()>(
                    &format!("response_{}", body_str_hash),
                    serde_json::to_string(&res).unwrap(),
                    60 * 2, // 2 minutes expiration
                )
                .unwrap();
        });
    }

    return HttpResponse::Ok()
        .insert_header(header::ContentType::plaintext())
        .body(serde_json::to_string(&res).unwrap());
}
